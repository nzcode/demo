//Swagger generated file
export interface InitiativeFieldSettingsDto {
  initiativeNumber?: FieldAccessibilityDto | undefined;
  initiativeTitle?: FieldAccessibilityDto | undefined;
  ideaGenerator?: FieldAccessibilityDto | undefined;
  initiativeDescription?: FieldAccessibilityDto | undefined;
  initiativeclassification?: FieldAccessibilityDto | undefined;
  wayofcreation?: FieldAccessibilityDto | undefined;
  businessProcess?: FieldAccessibilityDto | undefined;
  procedure?: FieldAccessibilityDto | undefined;
  processSME?: FieldAccessibilityDto | undefined;
  directorate?: FieldAccessibilityDto | undefined;
  initiativeOwner?: FieldAccessibilityDto | undefined;
  responsibleSponsor?: FieldAccessibilityDto | undefined;
  authorizingSponsor?: FieldAccessibilityDto | undefined;
  prioritizationScore?: FieldAccessibilityDto | undefined;
  initiativeType?: FieldAccessibilityDto | undefined;
  initiativeDriver?: FieldAccessibilityDto | undefined;
  benefitcategory?: FieldAccessibilityDto | undefined;
  costCategory?: FieldAccessibilityDto | undefined;
  estimatedValue?: FieldAccessibilityDto | undefined;
  reOccurringbenefit?: FieldAccessibilityDto | undefined;
  psaCostObject?: FieldAccessibilityDto | undefined;
  contractNumber?: FieldAccessibilityDto | undefined;
  reportingbucket?: FieldAccessibilityDto | undefined;
  expecteddeliverydate?: FieldAccessibilityDto | undefined;
  planneddeliverydate?: FieldAccessibilityDto | undefined;
  problemStatement?: FieldAccessibilityDto | undefined;
  goalStatement?: FieldAccessibilityDto | undefined;
  attachments?: FieldAccessibilityDto | undefined;
  comments?: FieldAccessibilityDto | undefined;
  commentsHistory?: FieldAccessibilityDto | undefined;
  counterMeasureRisk?: FieldAccessibilityDto | undefined;
  costController?: FieldAccessibilityDto | undefined;
  resultStatement?: FieldAccessibilityDto | undefined;
  firstForecast?: FieldAccessibilityDto | undefined;
  workflowButtons?: FieldAccessibilityDto | undefined;
  exectutionStatus?: FieldAccessibilityDto | undefined;
  milestone?: FieldAccessibilityDto | undefined;
  saveButton?: FieldAccessibilityDto | undefined;
  assesment?: FieldAccessibilityDto | undefined;
  costReporter?: FieldAccessibilityDto | undefined;
  l0PlannedStartDate?: FieldAccessibilityDto | undefined;
  l0PlannedEndDate?: FieldAccessibilityDto | undefined;
  l1PlannedStartDate?: FieldAccessibilityDto | undefined;
  l1PlannedEndDate?: FieldAccessibilityDto | undefined;
  l2PlannedStartDate?: FieldAccessibilityDto | undefined;
  l2PlannedEndDate?: FieldAccessibilityDto | undefined;
  submitButtonTitle?: string | undefined;
  rejectButton?: FieldAccessibilityDto | undefined;
}

export interface FieldAccessibilityDto {
  isVisible?: boolean;
  isReadonly?: boolean;
  isRequired?: boolean;
}
