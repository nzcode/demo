using CompanyName.Idea.Domain.Enums;

namespace CompanyName.Idea.Application.FieldSettings
{
    public interface IFieldsSettingsService
    {
        InitiativeFieldSettings GetSettings(InitiativeWorkflowStatus status);
    }
}
