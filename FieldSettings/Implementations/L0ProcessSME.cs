namespace CompanyName.Idea.Application.FieldSettings.Implementations
{
    public sealed class L0ProcessSME : InitiativeFieldSettings
    {
        public override IFieldAccessibility InitiativeTitle
        { get => SettngVariations.MR; set { } }

        public override IFieldAccessibility IdeaGenerator
        { get => SettngVariations.V; set { } }
    }
}
