namespace CompanyName.Idea.Application.FieldSettings.Implementations
{
    public sealed class L0PortfolioLead : InitiativeFieldSettings
    {
        public override IFieldAccessibility InitiativeTitle
        { get => SettngVariations.MR; set { } }

        public override IFieldAccessibility IdeaGenerator
        { get => SettngVariations.MR; set { } }
    }
}
