using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using CompanyName.Idea.Domain.Enums;

namespace CompanyName.Idea.Application.FieldSettings.Queries.GetByInitiativeStatus
{
    public record GetByInitiativeStatusQuery(InitiativeWorkflowStatus Status) : IRequest<InitiativeFieldSettingsDto>;

    public record GetByInitiativeStatusQueryHandler : IRequestHandler<GetByInitiativeStatusQuery, InitiativeFieldSettingsDto>
    {
        private readonly InitiativeFieldsSettingsService _fieldService;
        private readonly IMapper _mapper;

        public GetByInitiativeStatusQueryHandler(InitiativeFieldsSettingsService fieldService, IMapper mapper)
        {
            _fieldService = fieldService ?? throw new ArgumentNullException(nameof(fieldService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        public Task<InitiativeFieldSettingsDto> Handle(GetByInitiativeStatusQuery request, CancellationToken cancellationToken)
        {
            var instance = _fieldService.GetSettings(request.Status);

            var result = _mapper.Map<InitiativeFieldSettingsDto>(instance);
            return Task.FromResult(result);
        }
    }
}
