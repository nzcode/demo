using AutoMapper;
using CompanyName.Idea.Application.Common.Mappings;

namespace CompanyName.Idea.Application.FieldSettings
{
    public class FieldAccessibilityDto : IMapFrom<IFieldAccessibility>
    {
        public bool IsVisible { get; set; }

        public bool IsReadonly { get; set; }

        public bool IsRequired { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<IFieldAccessibility, FieldAccessibilityDto>();
        }
    }
}
