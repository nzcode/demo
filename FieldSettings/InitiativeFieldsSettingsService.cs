using System;
using CompanyName.Idea.Application.FieldSettings.Implementations;
using CompanyName.Idea.Domain.Enums;

namespace CompanyName.Idea.Application.FieldSettings
{
    public class InitiativeFieldsSettingsService : IFieldsSettingsService
    {
        public InitiativeFieldSettings GetSettings(InitiativeWorkflowStatus status)
        {
            switch (status)
            {
                case InitiativeWorkflowStatus.L0ProgramLead:
                    return new L0PortfolioLead();

                default:
                    throw new InvalidOperationException("no such setteting for workflow status");
            }
        }
    }
}
