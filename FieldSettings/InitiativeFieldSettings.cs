namespace CompanyName.Idea.Application.FieldSettings
{
    public abstract class InitiativeFieldSettings
    {
        public virtual IFieldAccessibility InitiativeNumber
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility InitiativeTitle
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility IdeaGenerator
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility InitiativeDescription
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility Initiativeclassification
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility Wayofcreation
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility BusinessProcess
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility Procedure
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility ProcessSME
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility Directorate
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility InitiativeOwner
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility ResponsibleSponsor
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility AuthorizingSponsor
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility PrioritizationScore
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility InitiativeType
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility InitiativeDriver
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility Benefitcategory
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility CostCategory
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility EstimatedValue
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility ReOccurringbenefit
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility PSACostObject
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility ContractNumber
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility Reportingbucket
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility Expecteddeliverydate
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility Planneddeliverydate
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility ProblemStatement
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility GoalStatement
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility Attachments
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility Comments
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility CommentsHistory
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility CounterMeasureRisk
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility CostController
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility ResultStatement
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility FirstForecast
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility WorkflowButtons
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility ExectutionStatus
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility Milestone
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility SaveButton
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility Assesment
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility CostReporter
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility L0PlannedStartDate
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility L0PlannedEndDate
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility L1PlannedStartDate
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility L1PlannedEndDate
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility L2PlannedStartDate
        { get; set;
        } = SettngVariations.N;

        public virtual IFieldAccessibility L2PlannedEndDate
        { get; set;
        } = SettngVariations.N;

        public virtual string SubmitButtonTitle { get; set; } = "submit";

        public virtual IFieldAccessibility RejectButton
        { get; set;
        } = SettngVariations.N;
    }
}
