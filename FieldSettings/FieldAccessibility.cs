namespace CompanyName.Idea.Application.FieldSettings
{
    public interface IFieldAccessibility
    {
        public bool IsVisible { get; }
        public bool IsReadonly { get; }
        public bool IsRequired { get; }
    }

    public class Modify : IFieldAccessibility
    {
        public bool IsVisible => true;

        public bool IsReadonly => false;

        public bool IsRequired => false;
    }

    public class Required : IFieldAccessibility
    {
        public bool IsVisible => true;

        public bool IsReadonly => false;

        public bool IsRequired => true;
    }

    public class ViewOnly : IFieldAccessibility
    {
        public bool IsVisible => true;

        public bool IsReadonly => true;

        public bool IsRequired => false;
    }

    public class None : IFieldAccessibility
    {
        public bool IsVisible => false;

        public bool IsReadonly => true;

        public bool IsRequired => false;
    }

    public static class SettngVariations
    {
        public static Modify M => new Modify();
        public static Required MR => new Required();
        public static ViewOnly V => new ViewOnly();
        public static None N => new None();
    }
}
