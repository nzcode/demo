using AutoMapper;
using CompanyName.Idea.Application.Common.Mappings;
using CompanyName.Idea.Application.FieldSettings.Implementations;

namespace CompanyName.Idea.Application.FieldSettings
{
    public class InitiativeFieldSettingsDto : IMapFrom<InitiativeFieldSettings>
    {
        public FieldAccessibilityDto InitiativeNumber { get; set; }

        public FieldAccessibilityDto InitiativeTitle { get; set; }

        public FieldAccessibilityDto IdeaGenerator { get; set; }

        public FieldAccessibilityDto InitiativeDescription { get; set; }

        public FieldAccessibilityDto Initiativeclassification { get; set; }

        public FieldAccessibilityDto Wayofcreation { get; set; }

        public FieldAccessibilityDto BusinessProcess { get; set; }

        public FieldAccessibilityDto Procedure { get; set; }

        public FieldAccessibilityDto ProcessSME { get; set; }

        public FieldAccessibilityDto Directorate { get; set; }

        public FieldAccessibilityDto InitiativeOwner { get; set; }

        public FieldAccessibilityDto ResponsibleSponsor { get; set; }

        public FieldAccessibilityDto AuthorizingSponsor { get; set; }

        public FieldAccessibilityDto PrioritizationScore { get; set; }

        public FieldAccessibilityDto InitiativeType { get; set; }

        public FieldAccessibilityDto InitiativeDriver { get; set; }

        public FieldAccessibilityDto Benefitcategory { get; set; }

        public FieldAccessibilityDto CostCategory { get; set; }

        public FieldAccessibilityDto EstimatedValue { get; set; }

        public FieldAccessibilityDto ReOccurringbenefit { get; set; }

        public FieldAccessibilityDto PSACostObject { get; set; }

        public FieldAccessibilityDto ContractNumber { get; set; }

        public FieldAccessibilityDto Reportingbucket { get; set; }

        public FieldAccessibilityDto Expecteddeliverydate { get; set; }

        public FieldAccessibilityDto Planneddeliverydate { get; set; }

        public FieldAccessibilityDto ProblemStatement { get; set; }

        public FieldAccessibilityDto GoalStatement { get; set; }

        public FieldAccessibilityDto Attachments { get; set; }

        public FieldAccessibilityDto Comments { get; set; }

        public FieldAccessibilityDto CommentsHistory { get; set; }

        public FieldAccessibilityDto CounterMeasureRisk { get; set; }

        public FieldAccessibilityDto CostController { get; set; }

        public FieldAccessibilityDto ResultStatement { get; set; }

        public FieldAccessibilityDto FirstForecast { get; set; }

        public FieldAccessibilityDto WorkflowButtons { get; set; }

        public FieldAccessibilityDto ExectutionStatus { get; set; }

        public FieldAccessibilityDto Milestone { get; set; }

        public FieldAccessibilityDto SaveButton { get; set; }

        public FieldAccessibilityDto Assesment { get; set; }

        public FieldAccessibilityDto CostReporter { get; set; }

        public FieldAccessibilityDto L0PlannedStartDate { get; set; }

        public FieldAccessibilityDto L0PlannedEndDate { get; set; }

        public FieldAccessibilityDto L1PlannedStartDate { get; set; }

        public FieldAccessibilityDto L1PlannedEndDate { get; set; }

        public FieldAccessibilityDto L2PlannedStartDate { get; set; }

        public FieldAccessibilityDto L2PlannedEndDate { get; set; }

        public string SubmitButtonTitle { get; set; }

        public FieldAccessibilityDto RejectButton { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<L0PortfolioLead, InitiativeFieldSettingsDto>();
            profile.CreateMap<L0ProcessSME, InitiativeFieldSettingsDto>();
        }
    }
}
