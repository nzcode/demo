#README#

I have a form that is used in a workflow.
The form has ~50 fields that have different accessibility depending on the step, the user who sees the form, and some other business logic.

I implemented a fabric named FieldSettngsService that creates concrete implementation with behavior for each business case.
Later the concrete class maps to TypeScript DTO that is being reused on the frontend part of the application. 
The frontend client can send back command for the form update, and the same implementation of FieldSettng is used for validation during fields mutation.