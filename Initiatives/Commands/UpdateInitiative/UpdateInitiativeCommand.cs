using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation.Results;
using MediatR;
using CompanyName.Idea.Application.Common.Exceptions;
using CompanyName.Idea.Application.EstimatedValues;
using CompanyName.Idea.Application.FieldSettings;
using CompanyName.Idea.Domain.Entities;
using CompanyName.Idea.Domain.Enums;

namespace CompanyName.Idea.Application.Initiatives.Commands.UpdateInitiative
{
    public class UpdateInitiativeCommand : IRequest
    {
        public Guid? InitiativeId { get; init; }

        public string Title { get; init; }

        public string Description { get; init; }

        public InitiativeClassification? Classification { get; init; }

        public Guid? ProcedureId { get; init; }

        public Guid? DirectorateId { get; init; }

        public string InitiativeOwnerNK { get; init; }

        public string ResponsibleSponsorNK { get; init; }

        public string AuthorizingSponsorNK { get; init; }

        public InitiativeType? InitiativeType { get; init; }

        public InitiativeDriver? InitiativeDriver { get; init; }

        public Guid? BenefitCategoryId { get; init; }

        public Guid? CostCategoryId { get; init; }

        public List<EstimatedValueDto> EstimatedValues { get; init; }

        public bool? IsReoccuringBenefit { get; init; }

        public string PSACostObject { get; init; }

        public string ContractNumber { get; init; }

        public string ReportingBucket { get; init; }

        public DateTime? ExpectedDeliveryDate { get; init; }

        public string ProblemStatement { get; init; }

        public string GoalStatement { get; init; }

        public string CounterMeasureRisk { get; init; }

        public string CostControllerNk { get; init; }

        public string ResultStatement { get; init; }

        public Guid? AssesmentId { get; init; }

        public InitiativeStatus? Status { get; init; }

        public DateTime? L0PlannedStart { get; init; }

        public DateTime? L0PlannedEnd { get; init; }

        public DateTime? L1PlannedStart { get; init; }

        public DateTime? L1PlannedEnd { get; init; }

        public DateTime? L2PlannedStart { get; init; }

        public DateTime? L2PlannedEnd { get; init; }

        public bool? UpdateStatus { get; set; } = false;
    }

    public class UpdateInitiativeCommandHandler : IRequestHandler<UpdateInitiativeCommand>
    {
        private readonly IFieldsSettingsService _fieldsSettingsService;

        public UpdateInitiativeCommandHandler(IFieldsSettingsService fieldsSettingsService)
        {
            _fieldsSettingsService = fieldsSettingsService;
        }

        public async Task<Unit> Handle(UpdateInitiativeCommand request, CancellationToken cancellationToken)
        {
            var init = new Initiative() { WorkflowStatus = InitiativeWorkflowStatus.L0ProgramLead };
            var fieldSettings = _fieldsSettingsService.GetSettings(init.WorkflowStatus);

            HandleField(fieldSettings.InitiativeTitle,
                () =>
                {
                    if (string.IsNullOrEmpty(request.Title))
                    {
                        ThrowIfNull(nameof(request.Title));
                    }

                    init.Title = request.Title;
                },
                () =>
                {
                    init.Title = request.Title;
                });

            HandleField(fieldSettings.InitiativeDescription,
                () =>
                {
                    if (string.IsNullOrEmpty(request.Description))
                    {
                        ThrowIfNull(nameof(request.Description));
                    }

                    init.Description = request.Description;
                },
                () =>
                {
                    init.Description = request.Description;
                });

            //other logic...

            return Unit.Value;
        }

        private async Task HandleFieldAsync(IFieldAccessibility field, Func<Task> tryModify, Func<Task> modify)
        {
            switch (field)
            {
                case Modify:
                    await tryModify();
                    break;

                case Required:
                    await modify();
                    break;

                case None:
                    break;

                default:
                    throw new InvalidOperationException("No such IFieldAccessibility");
            }
        }

        private void HandleField(IFieldAccessibility field, Action tryModify, Action modify)
        {
            switch (field)
            {
                case Modify:
                    tryModify();
                    break;

                case Required:
                    modify();
                    break;

                case None:
                    break;

                default:
                    throw new InvalidOperationException("No such IFieldAccessibility");
            }
        }

        public void ThrowIfNull(string fieldname)
        {
            throw new ValidationException(new List<ValidationFailure>() { new ValidationFailure(fieldname, "is null") });
        }
    }
}
